Droits d’auteur et reproduction :

Tout dépôt est en «Creative Commons» BY-NC-SA 3.0 France : Attribution 
– Pas d’Utilisation Commerciale 
– Partage dans les Mêmes Conditions 3.0.
[ http://creativecommons.org/licenses/by-nc-sa/3.0/fr/ ]